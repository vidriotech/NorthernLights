function dataStruct = viewPhotobleachData(directory)
    folderDirectoryStruct = dir(directory);
    fileNames = {folderDirectoryStruct.name};
    dataStruct = [];

    for i = 1:numel(fileNames)
       if numel(fileNames{i}) < 8
          fileNames{i} = []; 
       elseif ~strcmp(fileNames{i}(end-7:end), '.pmt.dat') && ~strcmp(fileNames{i}(end-8:end), '.meta.txt')
           fileNames{i} = [];
       elseif strcmp(fileNames{i}(1:end-9), 'NorthernLightsCfg')
           fileNames{i} = [];
       end
    end
    
    % Remove Empty Cells
    fileNames = fileNames(~cellfun(@isempty, fileNames, 'UniformOutput', true));
    
    for i = 1:numel(fileNames)
        fprintf('File %d of %d\n', i, numel(fileNames));
        % Find the right file
        if strcmp(fileNames{i}(end-7:end), '.pmt.dat')
            fileName = fileNames{i}(1:end-8);
            % Make sure this is a colony file
            colony = strfind(fileName, 'Colony_');
            colonyEnd = strfind(fileName, '_X');
            if isempty(colony)
                break;
            else
                % Get the colony name/number i.e. Colony_n_of_N
                colonyName = fileName(colony:colonyEnd-1);
                % If it is not a field in the data struct, make one
                if ~isfield(dataStruct, colonyName)
                    dataStruct.(colonyName) = [];
                end
            end
            
            % We have good colony file with a data struct
            
            pass = strfind(fileName, 'Pass_');
            
            if isempty(pass)
               break; 
            else
                xPos = fileName(strfind(fileName, '_X')+2:strfind(fileName, '_Y')-1);
                yPos = fileName(strfind(fileName, '_Y')+2:pass-2);
%                 yPos = fileName(strfind(fileName, '_Y')+2:strfind(fileName, '_Pass')-1);
                coords = [str2double(xPos), str2double(yPos)];

                if ~isfield(dataStruct.(colonyName), 'coordinates')
                    dataStruct.(colonyName).coordinates = coords;
                end
                
               passName = fileName(pass:pass+10);
               if ~isfield(dataStruct.(colonyName), passName)
                   dataStruct.(colonyName).(passName) = [];
               end
            end
            
            power = fileName(strfind(fileName, 'at_')+3:end-1);
            
            if ~isfield(dataStruct.(colonyName).(passName), 'power')
                dataStruct.(colonyName).(passName).power = power;
            end
            
            [~,pmtData,~,~] = scanimage.util.readLineScanDataFiles(fullfile(directory, fileName));
            
            
            dataStruct.(colonyName).(passName).pmtData = pmtData;
%             pmtData = [];
            if ~isfield(dataStruct.(colonyName), 'All_Pass_Data')
                dataStruct.(colonyName).All_Pass_Data = [];
            end
            dataStruct.(colonyName).All_Pass_Data = [dataStruct.(colonyName).All_Pass_Data; pmtData];
            pmtData = [];
        end
    end
end