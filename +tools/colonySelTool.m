classdef colonySelTool < tools.tool
    
    properties
        initialToolOutline = matlab.graphics.primitive.Surface.empty(0,1);
        
        toolCenterX = 0;
        toolCenterY = 0;
        
        toolSizeX = 9;
        toolSizeY = 9;
    end
    
    events
        deactivated;
    end
    
    methods
        function obj = colonySelTool(hView)
            obj = obj@tools.tool(hView);
            obj.activateTool();
        end
        
        function delete(obj)
            obj.deactivateTool();
            delete(obj.initialToolOutline);
        end
        
    end
    
    methods
        function activateTool(obj)
            obj.hAxes.ButtonDownFcn = @obj.drawArea;
            obj.hFig.Pointer = 'crosshair';
            obj.disableHitTest();
        end
        
        function deactivateTool(obj)
            obj.hFig.Pointer = 'arrow';
            set(obj.hFig,'WindowButtonMotionFcn',[],'WindowButtonUpFcn',[]);
            set(obj.hAxes, 'ButtonDownFcn',[]);
            
            obj.enableHitTest();
            
            notify(obj, 'deactivated');
         end
        
        % Disable or enable the snapsurf hittest
        function disableHitTest(obj)
            for i = 1:numel(obj.hView.hSSSurfs)
                obj.hView.hSSSurfs(i).HitTest = 'off';
            end
        end
        
        function enableHitTest(obj)
            for i = 1:numel(obj.hView.hSSSurfs)
                obj.hView.hSSSurfs(i).HitTest = 'on';
            end
        end
        
        function apply(obj, center, bounds)
            obj.hView.hNL.addScanColoniesFromTool(center, bounds);
        end
        
    end
    
    methods
        function drawArea(obj, stop, varargin)
            persistent oppt;
            persistent ocenterxy;
            persistent centerxy;
            persistent hsz;
            
            if nargin > 2
                oppt = getPointerLocation(obj.hAxes);
                
                hsz = [100 100]/2; 
                centerxy = hsz;
                
                handleLen = diff(obj.hAxes.YLim) / 40;

                pts = [centerxy-hsz; centerxy+[-hsz(1) hsz(2)]; centerxy+[hsz(1) -hsz(2)]; centerxy+hsz; centerxy; centerxy-[0 hsz(2)+handleLen]];
                rot = 0;
                R = [cos(rot) sin(rot) 0;-sin(rot) cos(rot) 0; 0 0 1];
                pts = scanimage.mroi.util.xformPoints(pts,R);
                pts = pts + repmat(oppt,6,1);
                ocenterxy = pts(5,:);
                centerxy = ocenterxy;

                xx = [pts(1:2,1) pts(3:4,1)];
                yy = [pts(1:2,2) pts(3:4,2)];
                
                % Zdata of outline must be within axes Z lim. 
                axesZLim = obj.hAxes.ZLim;
                obj.initialToolOutline = surface(xx, yy, ones(2),'FaceColor','none','edgecolor','blue','linewidth',1,'parent',obj.hAxes);
                zLimRange = min(axesZLim):0.0001:max(axesZLim);
                idx = floor(numel(zLimRange)/2)+1;
                obj.initialToolOutline.ZData = zLimRange(idx)*ones(2);
                    
                set(obj.hFig,'WindowButtonMotionFcn',@(varargin)obj.drawArea(false),'WindowButtonUpFcn',@(varargin)obj.drawArea(true));
                waitfor(obj.hFig,'WindowButtonMotionFcn',[]);
            elseif stop
                
                obj.deactivateTool();
                
                sz = hsz*2;
                
                obj.toolCenterX = centerxy(1);
                obj.toolCenterY = centerxy(2);
                               
                obj.toolSizeX = sz(1);
                obj.toolSizeY = sz(2);
                
                
                obj.updateInitTile();
                
                
                obj.apply([obj.toolCenterX obj.toolCenterY], [obj.toolSizeX obj.toolSizeY]);
                
                if isvalid(obj.initialToolOutline)
                    most.idioms.safeDeleteObj(obj.initialToolOutline);
                end
                
                
            else
                nwpt = getPointerLocation(obj.hAxes);
                
                centerxy = (oppt+nwpt)/2;
                
                relpt = nwpt - centerxy;
                rot = 0;
                R = [cos(rot) sin(rot) 0;-sin(rot) cos(rot) 0; 0 0 1];
                hsz = abs(scanimage.mroi.util.xformPoints(relpt,R,true));
                
                %find new points
                handleLen = diff(obj.hAxes.YLim) / 40;
                pts = [-hsz; -hsz(1) hsz(2); hsz(1) -hsz(2); hsz; 0 0; 0 -hsz(2)-handleLen];
                rot = 0;
                R = [cos(rot) sin(rot) 0;-sin(rot) cos(rot) 0; 0 0 1];
                pts = scanimage.mroi.util.xformPoints(pts,R);
                pts = pts + repmat(centerxy,6,1);

                xx = [pts(1:2,1) pts(3:4,1)];
                yy = [pts(1:2,2) pts(3:4,2)];
                obj.initialToolOutline.XData = xx;
                obj.initialToolOutline.YData = yy;
            end
        end
        
        function updateInitTile(obj)
            if isvalid(obj.initialToolOutline) && isa(obj.initialToolOutline, 'matlab.graphics.primitive.Surface')
                % 1) Update Center
                p = [obj.toolCenterX obj.toolCenterY 0];
                
                % 2) Update Size
                sizeXum = obj.toolSizeX;
                sizeYum = obj.toolSizeY;
                [xx, yy] = meshgrid([-.5 .5], [-.5 .5]);
                ScannerToMotorSpace = [sizeXum 0 0; 0 sizeYum 0; 0 0 1];
                [surfMeshXX, surfMeshYY] = scanimage.mroi.util.xformMesh(xx,yy,ScannerToMotorSpace);
                
                % 3) Commit Changes to Surf
                cData = nan;
                xdata = p(1) + surfMeshXX;
                ydata = p(2) + surfMeshYY;
                zdata = p(3)*ones(2);
                
                obj.initialToolOutline.XData = xdata;
                obj.initialToolOutline.YData = ydata;
                obj.initialToolOutline.ZData = zdata;
                obj.initialToolOutline.CData = cData;
                
                obj.initialToolOutline.Visible = 'off';
            else
                % Create surface?
            end
            
        end
    end
    
end

function pt = getPointerLocation(hAx)
    pt = hAx.CurrentPoint(1, 1:2);
end