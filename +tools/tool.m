classdef tool < handle
    
        %% Local Properties
    properties
        hFig;
        hAxes;
        hView;
        
        hListenersTool = event.listener.empty();
        hCurrentObjectListener = event.listener.empty();
        
        hCurrentObject = [];
        hCurrentObjectKeyPressFcn = [];
    end
    
    methods
        % Tile Tool operates on the tile view
        function obj = tool(hView)
            obj.hView = hView;
            obj.hFig = hView.hFig;
            obj.hAxes = hView.hFovAxes;
            
            obj.hListenersTool(end+1) = addlistener(obj.hView,'ObjectBeingDestroyed',@(varargin)obj.delete);
            obj.hCurrentObjectListener(end+1) = addlistener(obj.hFig,'CurrentObject','PostSet',@(varargin)obj.focusChanged);
            obj.setKeyPressFcn();
        end
        
        function delete(obj)
            delete(obj.hListenersTool);
            delete(obj.hCurrentObjectListener);
            
            if most.idioms.isValidObj(obj.hCurrentObject)
                obj.hCurrentObject.KeyPressFcn = obj.hCurrentObjectKeyPressFcn;
            end
        end
        
        function focusChanged(obj)
            if obj.hFig.CurrentObject ~= obj.hAxes
                obj.delete(); % if users presses any other ui element, delete tool
            else            
                obj.setKeyPressFcn();
            end
        end
        
        function setKeyPressFcn(obj)
            if most.idioms.isValidObj(obj.hCurrentObject)
                obj.hCurrentObject.KeyPressFcn = obj.hCurrentObjectKeyPressFcn;
            end
            
            currentObject = obj.hFig.CurrentObject;
            if isempty(currentObject) || ~isprop(currentObject,'KeyPressFcn')
                currentObject = obj.hFig;
            end
            
            obj.hCurrentObject = currentObject;
            obj.hCurrentObjectKeyPressFcn = currentObject.KeyPressFcn;
            
            obj.hCurrentObject.KeyPressFcn = @obj.keyPressed;
        end
        
        function keyPressed(obj,src,evt)
            switch evt.Key
                case 'escape'
                    obj.delete();
            end
        end
    end
    
    methods (Static)
        function toolClasses = findAllTools()
            toolsPath = mfilename('fullpath');
            toolsPath = fileparts(toolsPath);
            thisClassName = mfilename('class');
            toolClasses = most.util.findAllClasses(toolsPath,thisClassName);
            
            isAbstractMask = false(size(toolClasses));
            for idx = 1:numel(toolClasses)
                mc = meta.class.fromName(toolClasses{idx});
                isAbstractMask(idx) = mc.Abstract;
            end
            
            toolClasses(isAbstractMask) = [];
        end
    end
end