function [centroids, convexHulls] = findcolonies(im, diamThresh, solThresh)

%     RMS = rmsCalc(im);
%     threshold = mean(RMS(:));
%     imb=im>threshold;
%     imb=im>600;
    
%     imb = im2bw(im);%imbinarize(im);
    imb = im;
    props = regionprops('table', imb, 'ConvexHull', 'EquivDiameter', 'Solidity', 'Centroid');

    convexHulls = cell(0, 1);
    centroids = zeros(0, 2);
    for iprop = 1:size(props, 1)
        prop = props(iprop,:);
        sol = prop.Solidity;
        if sol >= solThresh && prop.EquivDiameter > diamThresh
            centroids(end+1,:) = prop.Centroid;
            convexHulls{end+1} = prop.ConvexHull{1};
        end
    end % for
end % function