classdef NorthernLightsColonyPicker < handle

    properties
        hFig;
        hFovPanel;
        hFovAxes;
        hSSSurfs = matlab.graphics.primitive.Surface.empty;
        ssImgData;
        metadata;
        colonyCursors = matlab.graphics.primitive.Line.empty;
    end

    properties (SetObservable)
        binThresh = 0.4;
        solThresh = 0.8;
        diamThresh = 10;
    end

    properties (Hidden)
        maxFov = 100000;
        defaultFovSize = 100000;
        currentFovSize = 100000;
        currentFovPos = [0 0];
        numColonies;
        
        hIntensitySlider;
        hSoliditySlider;
        hDiameterSlider;
    end


    %% Lifecycle
    methods
        function obj = NorthernLightsColonyPicker(dataDir)
            if nargin < 1
                dataDir = uigetdir(pwd, 'Select Dataset Directory...');
            end

            obj.ssImgData = load(fullfile(dataDir, 'SnapShotImages.mat'));
            obj.metadata = most.json.loadjson(fullfile(dataDir, 'NorthernLightsCfg.meta.txt'));
            obj.numColonies = size(obj.metadata.colonyPositions, 1);

            obj.hFig = figure('numbertitle', 'off', 'name', 'NorthernLights Colony Picker',...
                'units', 'pixels', 'menubar', 'none',...
                'position', most.gui.centeredScreenPos([1600 800], 'pixels'),...
                'CloseRequestFcn', @(varargin)obj.delete);
            mainFlow = most.gui.uiflowcontainer('Parent', obj.hFig,...
                'FlowDirection', 'TopDown', 'Margin' ,0.0001);
             
            obj.hFovPanel = uipanel('parent', mainFlow, 'bordertype', 'none');            
            obj.hFovAxes = axes('parent', obj.hFovPanel, 'box', 'off', 'Color', 'k',...
                'GridColor', .9*ones(1,3), 'ButtonDownFcn' ,@obj.mainViewPan,...
                'xgrid', 'on', 'ygrid', 'on', 'GridAlpha', .25, 'XTickLabel', [],...
                'YTickLabel', [], 'units', 'normalized', 'position', [0 0 1 1]);
            
            obj.hFovPanel.SizeChangedFcn = @obj.updateFovLims;
            obj.hFig.WindowScrollWheelFcn = @obj.scrollWheelFcn;

            bottomFlow = most.gui.uiflowcontainer('Parent', mainFlow,...
                'FlowDirection', 'LeftToRight', 'HeightLimits', 30);
            
            % imbinarize threshold
            most.gui.staticText('Parent', bottomFlow, 'String', 'Intensity',...
                'HorizontalAlignment', 'left', 'FontSize', 10, 'WidthLimits', 50);
            obj.hIntensitySlider = most.gui.slider('parent', bottomFlow, 'WidthLimits', 100,...
                'Bindings', {obj 'binThresh' 1});
            most.gui.uicontrol('parent', bottomFlow, 'style', 'edit', 'FontSize', 10, 'WidthLimits', 60,...
                'Bindings', {obj 'binThresh' 'value'}, 'callback', @(varargin)obj.findColonies);
            
            % regionprops 'Solidity' threshold
            most.gui.staticText('Parent', bottomFlow, 'String', 'Solidity',...
                'HorizontalAlignment', 'left', 'FontSize', 10, 'WidthLimits', 50);
            obj.hSoliditySlider = most.gui.slider('parent', bottomFlow, 'WidthLimits', 100,...
                'Bindings', {obj 'solThresh' 1});
            most.gui.uicontrol('parent', bottomFlow, 'style', 'edit', 'FontSize', 10, 'WidthLimits', 60,...
                'Bindings', {obj 'solThresh' 'value'}, 'callback', @(varargin)obj.findColonies);
            
            % regionprops 'Diameter' threshold
            most.gui.staticText('Parent', bottomFlow, 'String', 'Diameter',...
                'HorizontalAlignment', 'left', 'FontSize', 10, 'WidthLimits', 60);
            obj.hSoliditySlider = most.gui.slider('parent', bottomFlow, 'WidthLimits', 100,...
                'min', 1, 'max', 100, 'units', 'pixels', 'Bindings', {obj 'diamThresh' 1});
            most.gui.uicontrol('parent', bottomFlow, 'style', 'edit', 'FontSize', 10, 'WidthLimits', 60,...
                'Bindings', {obj 'diamThresh' 'value'}, 'callback', @(varargin)obj.findColonies);
            
            % action button
            most.gui.uicontrol('parent' ,bottomFlow, 'string', 'Find', 'FontSize', 10, 'WidthLimits', 40,...
                'callback', @(varargin)obj.findColonies);
            
            [xx, yy] = meshgrid([-.5 .5],[-.5 .5]);
            [surfMeshXX, surfMeshYY] = scanimage.mroi.util.xformMesh(xx, yy, obj.ssImgData.cameraToStageTransform);
            
            if iscell(obj.ssImgData.imageData{1})
                ims = obj.ssImgData.imageData{1};
            else
                ims = obj.ssImgData.imageData;
            end

            if iscell(obj.ssImgData.stagePositions{1})
                posns = obj.ssImgData.stagePositions{1}(end-numel(ims)+1:end);
            else
                posns = obj.ssImgData.stagePositions;
            end

            for i=1:numel(ims)
                p = posns{i};
                obj.hSSSurfs(end+1) = surface('parent', obj.hFovAxes, 'xdata', p(1) + surfMeshXX,...
                    'ydata', p(2) + surfMeshYY, 'zdata', -1.1*ones(2), 'CData', ims{i},...
                    'FaceColor', 'texturemap', 'EdgeAlpha', 0, 'hittest', 'off'); 
            end
        end

        function delete(obj)
            delete(obj.hFig);
        end
    end

    %% User methods
    methods
        function findColonies(obj, varargin)
            if ~isempty(obj.colonyCursors)
                arrayfun(@(x) delete(x), obj.colonyCursors);
                obj.colonyCursors = [];
            end
            
            if iscell(obj.ssImgData.imageData{1})
                ims = obj.ssImgData.imageData{1};
            else
                ims = obj.ssImgData.imageData;
            end
            
            convexHulls = cell(size(obj.ssImgData.imageData{1}));
            centroids = cell(size(obj.ssImgData.imageData{1}));
            
            % find connected regions matching parameters
            for i=1:numel(ims)
                imb = imbinarize(ims{i});
                
                props = regionprops('table', imb, 'ConvexHull', 'EquivDiameter', 'Solidity', 'Centroid');
                convexHulls{i} = cell(0, 1);
                centroids{i} = zeros(0, 2);
                for j = 1:size(props, 1)
                    prop = props(j,:);
                    sol = prop.Solidity;
                    if sol >= obj.solThresh && prop.EquivDiameter > obj.diamThresh
                        convexHulls{i}{end+1} = prop.ConvexHull{1};
                        centroids{i}(end+1,:) = prop.Centroid;
                    end
                end
            end
            
            obj.ssImgData.convexHulls = convexHulls;
            obj.metadata.centroids = centroids;
            
            obj.transformCentroids();
            obj.rmPairwiseDupes();
            
            % find the number of all centroids per plate
            fcc = arrayfun(@(x) size(x{1}, 1), obj.metadata.centroids);
            obj.numColonies = sum(fcc);
            fccSum = cumsum(fcc);
            
            obj.metadata.foundColonies = zeros(obj.numColonies, 3);
            for i=1:numel(obj.metadata.centroids)
                imgCentroids = obj.metadata.centroids{i};
                if i > 1
                    obj.metadata.foundColonies(fccSum(i-1)+1:fccSum(i), 1:2) = imgCentroids;
                else
                    obj.metadata.foundColonies(1:fccSum(i), 1:2) = imgCentroids;
                end
            end
           
            obj.showColonies();
        end
        
        function rmPairwiseDupes(obj)
            centroids = obj.metadata.centroids;
            hulls = obj.ssImgData.convexHulls;
            
            for i=1:numel(centroids)
                c1 = centroids{i};
                md1 = min(pdist(c1)); % compute minimum distance between points in c1
                for j=1:numel(centroids)
                    if j == i, continue; end
                    c2 = centroids{j};
                    h2 = hulls{j};
                    md2 = min(pdist(c2)); % compute minimum distance between points in c2
                    md = min(md1, md2);
                                        
                    pd = pdist2(c1, c2);
                    [~, discard] = find(pd < md);
                    c2Keep = setdiff(1:size(c2,1), discard)';
                    centroids{j} = c2(c2Keep,:);
                    hulls{j} = h2(c2Keep);
                end
            end
            
            obj.metadata.centroids = centroids;
            obj.ssImgData.convexHulls = hulls;
        end
        
        function transformCentroids(obj)
            surfs = obj.hSSSurfs;
            
            for i = 1:numel(surfs)
                imSurf = surfs(i);
                xdat = imSurf.XData;
                ydat = imSurf.YData;
                cdat = imSurf.CData;
                
                Xul = xdat(1,1); Xur = xdat(1,2); Xll = xdat(2,1);
                Yul = ydat(1,1); Yur = ydat(1,2); Yll = ydat(2,1);
                X = size(cdat,2); Y = size(cdat,1);
                origin = [Xul; Yul];
                
                % explicit matrix -> surface coordinate transform
                T = [(Xur-Xul)/X (Xll-Xul)/Y; (Yur-Yul)/X (Yll-Yul)/Y];
                %Tinv = (-(Xll-Xul)*(-Yul+Yur)/(X*Y)+(-Xul+Xur)*(Yll-Yul)/(X*Y))^-1*[(Yll-Yul)/Y (-Xll+Xul)/Y; (Yul-Yur)/X (-Xul+Xur)/X];
                obj.metadata.centroids{i} = (T*obj.metadata.centroids{i}' + origin)';
                hulls = obj.ssImgData.convexHulls{i};
                obj.ssImgData.convexHulls{i} = cellfun(@(x) (T*x' + origin)', hulls, 'UniformOutput', 0);
            end
        end
        
        function showColonies(obj)
            foundColonies = obj.metadata.foundColonies;
            savedColonies = obj.metadata.colonyPositions;
            
            for i=1:size(foundColonies,1)
                p = foundColonies(i,:);
                obj.colonyCursors(end+1) = line('xdata', p(1), 'ydata', p(2), 'zdata', 5,...
                    'parent', obj.hFovAxes, 'Marker', '+', 'Color', 'r', 'MarkerSize', 20,...
                    'linewidth', 2, 'visible', 'on');
            end
            
            for i=1:size(savedColonies,1)
                p = savedColonies(i,:);
                obj.colonyCursors(end+1) = line('xdata', p(1), 'ydata', p(2), 'zdata', 5,...
                    'parent', obj.hFovAxes, 'Marker', '+', 'Color', 'k', 'MarkerSize', 20,...
                    'linewidth', 2, 'visible', 'on');
            end
        end
        
        function hideColonies(obj)
            arrayfun(@(x) set(x, 'visible', 'off'), obj.colonyCursors);
        end
    end
    

    %% Internal Methods
    methods
        function updateFovLims(obj, varargin)
            obj.hFovPanel.Units = 'pixels';
            p = obj.hFovPanel.Position;

            lm = 0.5 * obj.currentFovSize * p(3:4) / min(p(3:4));
            obj.hFovAxes.XLim = lm(1) * [-1 1] + obj.currentFovPos(1);
            obj.hFovAxes.YLim = lm(2) * [-1 1] + obj.currentFovPos(2);
            obj.hFovAxes.Units = 'normalized';
            obj.hFovAxes.Position = [0 0 1 1];
        end
        
        function scrollWheelFcn(obj,~,evt)
            opt = obj.hFovAxes.CurrentPoint([1 3]);
            obj.currentFovSize = obj.currentFovSize * 1.5^evt.VerticalScrollCount;
            obj.currentFovPos = obj.currentFovPos + opt - obj.hFovAxes.CurrentPoint([1 3]);
        end
        
        function mainViewPan(obj,~,evt)
            persistent opt

            if strcmp(evt.EventName, 'Hit') && (evt.Button == 1)
                opt = obj.hFovAxes.CurrentPoint([1 3]);
                set(obj.hFig,'WindowButtonMotionFcn',@obj.mainViewPan,'WindowButtonUpFcn',@obj.mainViewPan);
            elseif strcmp(evt.EventName, 'WindowMouseMotion')
                obj.currentFovPos = obj.currentFovPos + opt - obj.hFovAxes.CurrentPoint([1 3]);
            else
                set(obj.hFig,'WindowButtonMotionFcn',[],'WindowButtonUpFcn',[]);
            end
        end
        
        function set.currentFovSize(obj,v)
            obj.currentFovSize = max(min(v,obj.maxFov),obj.maxFov/1000);
            obj.currentFovPos = obj.currentFovPos;
        end

        function set.currentFovPos(obj,v)
            mxPos = (obj.maxFov-obj.currentFovSize);
            obj.currentFovPos = max(min(v,mxPos),-mxPos);
            obj.updateFovLims();
        end
        
        function set.binThresh(obj, val)
            obj.binThresh = round(val, 2);
        end
        function set.solThresh(obj, val)
            obj.solThresh = round(val, 2);
        end
        function set.diamThresh(obj, val)
            obj.diamThresh = round(val);
        end
    end
end