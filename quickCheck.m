function quickCheck(frameData)

    if max(frameData(:)) < 50
        disp('Cant Find Colony, data is just noise?');
    else
        filteredData = filter((5000/numel(frameData)), [1 (5000/numel(frameData))-1], frameData);
        threshold = rmsCalc(filteredData);

        valsAboveThreshold = find(filteredData >= threshold);

        start = valsAboveThreshold(1);
        stop = valsAboveThreshold(end);

        data = frameData;
        data(start:stop) = 1;
        data(1:start-1) = 0; 
        data(stop+1:end) = 0;

        top = max(diff(data));
        bottom = min(diff(data));

        topGood = (~isempty(top) && top == 1);
        bottomGood = (~isempty(bottom) && bottom == -1);

        if topGood && bottomGood
            disp('Z is good');
        elseif topGood && ~bottomGood
            disp('Top found, move deeper');
        elseif ~topGood && bottomGood
            disp('bottom found, too deep');
        elseif ~topGood && ~bottomGood
            disp('cant find');
        else
            disp('Unknown para-causal event');
        end
    end

    figure()
    plot(data);
end