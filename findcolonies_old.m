function [centers,diameters]=findcolonies_old(im,radiusMin,radiusMax,log10IntensityThreshold)

im2=imfilter(double(im),fspecial('gaussian',[19 19],3));
mask=im2>10^log10IntensityThreshold;

im2(~mask)=0;
props=regionprops(mask,'boundingbox');

centers=[];
radii=[];

for iprop=1:length(props)
    [rows,cols,origin,area]=bbox_to_range(props(iprop).BoundingBox,radiusMax*0.5,size(im2));
    if area>radiusMax*radiusMax
%         continue;
    end
    if ~isempty(rows) && ~isempty(cols)
        reg=im2(rows,cols);
        [c,r,~]=imfindcircles(reg,[radiusMin radiusMax],'method','twostage');
%         [row,col]=find(reg==max(reg(:)),1);
%         c=[col-0.5,row-0.5];
%         r=10;
        if ~isempty(c)
            centers=[centers;c+repmat(origin,size(c,1),1)];
            radii=[radii;r];
        end
    end
end

centers=centers';
diameters=2*radii';
end

function [rows,cols,origin,area]=bbox_to_range(bbox,pad,imsz)
pad=floor(pad);
bbox=floor(bbox);
origin=max([bbox(1)-pad-1 bbox(2)-pad-1],[0 0]);
corner=min([bbox(2)+pad+bbox(4),bbox(1)+pad+bbox(3)],imsz);
d=corner-origin;
area=d(1).*d(2);
rows=(origin(2)+1):corner(1);
cols=(origin(1)+1):corner(2);
end











