function [centers, diameters] = findColonesAlt(img, minRad, maxRad, threshold)

    % Gaussian Filter
    img = imfilter(double(img), fspecial('gaussian', [8 8], 8));
    
    % Binarize with Threshold
    img(img<=threshold) = 0;
    img(img>threshold) = 1;
    
    % Find Circles
    [c,r,~]=imfindcircles(img,[minRad maxRad],'method','twostage');
    
    centers = c;
    diameters = 2*r;
    
    % Confirmation testing
%     centers = fliplr(c);
%     cc = round(centers);
%     img2 = img;
%     for i = 1:length(cc)
%         rangeA = cc(i,1)-20:cc(i,1)+20; 
%         rangeA(rangeA <= 0) = 1;
%         rangeA(rangeA > 1008) = 1008;
%         img2(rangeA, cc(i,2)) = 1;
%         
%         rangeB = cc(i,2)-20:cc(i,2)+20;
%         rangeB(rangeB <= 0) = 1;
%         rangeB(rangeB > 1008) = 1008;
%         img2(cc(i,1), rangeB) = 1;
%     end
%     figure()
%     imshow(img2);
end