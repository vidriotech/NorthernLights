function compile(opt)
projname = 'NorthernLights';
projFilename = [projname '.prj'];
cmd_hdr_name = 'param.user.defined.mcr.options';
orig_prj_loc = fullfile('mcc', projFilename);

doDeploy = false;
if nargin > 0
switch opt
    case 'clean'
        if 2 == exist(projFilename, 'file')
            delete(projFilename);
        end
        
        if 2 == exist('launch', 'file')
            delete('launch.m');
        end
        scanimage.util.mcc.compile('clean');
        return;
    case 'deploy'
        doDeploy = true;
    otherwise
        error('undefined option `%s`', opt);
end
end

if ~doDeploy
    warning('Dry Run.  To compile please run with arg `deploy`');
end

%sets up and compiles northernlights
warning('off', 'ScanImage:Compile:DryRun');
scanimage.util.mcc.compile; %dry run
warning('on', 'ScanImage:Compile:DryRun');

%add ScanImage resource files into NorthernLights
resource_files = scanimage.util.mcc.constructFileList;

orig_fid = fopen(orig_prj_loc, 'r');
edit_fid = fopen(projFilename, 'W');
in_unset = false;
while true
    line = fgets(orig_fid);
    if ~ischar(line)
        break;
    end
    
    cmd_hdr = strfind(line, cmd_hdr_name);
    
    if isempty(cmd_hdr)
        unset_enable = strfind(line, '<unset>');
        unset_disable = strfind(line, '</unset>');
        if ~isempty(unset_enable)
            in_unset = true;
        elseif ~isempty(unset_disable)
            in_unset = false;
        end
        %just write back out
    elseif in_unset
        %unset <unset> for <param.user.defined.mcr.options>
        continue;
    else
        %write to <param.user.defined.mrc.options> manually
        rewrite_i = cmd_hdr + length(cmd_hdr_name);
        prefix = line(1:rewrite_i);
        args = ['-a "' strjoin(resource_files, ['"' 10 '-a "']) '"'];
        line = [prefix '>' args '</' cmd_hdr_name '>' 10];
    end
    fwrite(edit_fid, line, 'char');
end
fclose(orig_fid);
fclose(edit_fid);

if ~doDeploy
    return;
end

copyfile(fullfile('mcc', 'launch.m'));
deploytool -package NorthernLights;
end